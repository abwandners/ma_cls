\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ma}[2016/06/07 Mastertheses]

\LoadClass[a4paper,12pt,titlepage]{article}
\RequirePackage{titlesec}
\RequirePackage{xcolor}
\RequirePackage[a4paper]{geometry}
\RequirePackage{hyperref}
\RequirePackage{fancyhdr}

%\newgeometry{left=3cm,bottom=3cm,top=3cm,right=3cm}




\def\tname#1{\def\@tname{#1}}
\newcommand{\printtname}{\@tname}


\def\ttitle#1{\def\@ttitle{#1}}
\newcommand{\printttitle}{\@ttitle}

\pagestyle{fancy}
\fancyhf{}
\rhead{\scshape Seite \thepage}
\lhead{\scshape \rightmark}
\lfoot{Gew\"ahrleistung der Plattform-Sicherheit in einem\\
	eingebetteten System mithilfe von Software-Sandboxes}
\rfoot{\scshape Sebastian Abwandner}




\newcommand{\maketitlepage}{
	\newgeometry{left=2cm,bottom=4cm,top=3cm,right=2cm}
	
	\thispagestyle{empty}
	\begin{flushright}
		\includegraphics[scale=0.3]{./ma_cls/hslogo.eps}\\
	\end{flushright}
	
	\begin{minipage}[t]{0.78\textwidth}
		\Huge \textsf{\textbf{\textcolor{orange}{Masterarbeit}}}\\
		\Large \textsf{Studienrichtung Informatik}\\
	\end{minipage}%
	\hfill
	\begin{minipage}[t]{0.2\textwidth}
		{\textsf{\textcolor{orange}{Fakult\"at f\"ur\\ Informatik }}}
	\end{minipage}%
	
	\begin{flushleft}
		\vspace*{1cm}
		\huge
		\textsf{\textbf{\textcolor{orange}{\printtname}}\\
		\printttitle	}\\
		\vspace*{0.5cm}
	\end{flushleft}	
	\vfill
	\normalsize
	%\newcolumntype{x}[1]{>{\raggedleft\arraybackslash\hspace{0pt}}p{#1}}
	
	\begin{minipage}[t]{0.65\textwidth}
		%	\vspace*{0.05cm}
		
		%Dipl.-Inf. (FH), Dipl.-Designer (FH), Master of Arts Erich Seifert
		%\Large \textsf{Prüfer: Prof.~Dr.~Gordon~T.~Rohrmair}\\
		\Large \textsf{Pr\"ufer: \\Dipl.-Inf. (FH), Dipl.-Designer (FH),\\ Master of Arts Erich Seifert}\\
		\Large \textsf{Abgabe der Arbeit am: 25.10.2016}
	\end{minipage}%
	\hfill
	\begin{minipage}[t]{0.35\textwidth}
		\begin{small}
			\textsf{
				\textcolor{red}{
					Hochschule Augsburg\\
					University of Applied Sciences\\
					An der Hochschule 1\\
					D 86161 Augsburg\\
					Telefon +49 821 5586-0\\
					Fax +49 821 5586-3222\\
					\href{http://www.hs-augsburg.de}{www.hs-augsburg.de}\\
					\href{mailto:info@hs-augsburg.de}{info@hs-augsburg.de}}\\
				\\
				Fakult\"at f\"ur Informatik\\
				Telefon: +49 821 5586-3450\\
				Fax: +49 821 5586-3499\\
				\\
				Verfasser der Masterarbeit:\\
				Sebastian Abwandner\\
				Krumbacher Strasse 25\\
				86470 Thannhausen\\
				Telefon: +49 176 2281-8335\\
				\href{mailto:sebastiankonradarno.abwandner@hs-augsburg.de}{sebastiankonradarno.abwandner@hs-augsburg.de}\\
			}
		\end{small}
	\end{minipage}%
	\pagebreak
	\restoregeometry
	
}



\titleformat{\part}
{\pagebreak\thispagestyle{empty}\vspace*{\fill}\titlerule[1em]\vspace{1em}\Huge\scshape\raggedright\centering}
	{}{0em}
	{Teil \thepart\\}
	[\titlerule\vspace*{\fill}\pagebreak]


\titleformat{\section}
{}
{}{0em}
{\large\bfseries\scshape\thesection~}
[]

	
